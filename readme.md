# MDWebsite

## About
A simple Markdown based static website template that requires no building. Markdown is automatically rendered into the page via JavaScript.

## Features
- no install or building needed
- uses [cmarkd](https://gitlab.com/lucidlylogicole/cmarkd), a customized commonmark rendering engine
- easily customize and theme
- JavaScript is required to render the markdown
- links to markdown pages are automatically rendered (must use extension `.md`)
- links to other urls are rendered normally
- automatically builds a table of contents based on `h2,h3,h4,h5`

## Instructions
1. Download or clone this repo
2. Modify `index.md` to set up your homepage
3. Customize the template by editing `index.html`
4. Add other markdown pages and link to them with the `.md` extension


## Markdown Usage
- use only one `h1` and use it for the page title
- Links to markdown pages must use the `.md` extension, otherwise the link will be treated normally and not be rendered. Like:
    - `readme.md`
    - `articles/sample.md`
- the only real required html element is:
    - `<div id="md_content">`

## Customize

### Main Options
The main options can be customized at the top of the `index.html` file

    // Options
    const SiteOptions = {
        md_element:'md_content',    // Element to render markdown in
        tableofcontents:1,          // Show Table of Contents
        base_window_title:'',       // text of window title before page title
        allow_caching:0,            // Allow caching of the pages
        afterPageLoad:function(){}, // function called after markdown is rendered
    }

### Theme
Theme your website by adding a css reference after the `base.css` link in the `index.html` file

### Fully Customize
The whole of `index.html` can be customized to a desired layout. That file is used for all markdown rendering.

### Custom Rendering for File Extension
The `MDPage.renderMap` is a dictionary for mapping url file extensions to a rendering function.  To add an extension to be rendered:

    function mypyviewer(text) {
        document.getElementById(MDPage.md_element).innerText=text
    }
    MDPage.renderMap['py'] = mypyviewer

If you just want text displayed, you can use the default `textRender` function:

    MDPage.renderMap['py'] = MDPage.textRender

### Page Template Layout
The default layout
<table>
    <tr><td colspan=3>#header</td><tr>
    <tr style="height:100px;">
        <td>#left-container</td>
        <td style="width:300px;">#md_content</td>
        <td>#right-container</td>
    </tr>
    <tr><td colspan=3>#footer</td><tr>
</table>