
//---o:
let MDPage = function(options) {
let MDPage = {
    version:'1.1.0',
    md_element:options.md_element,

    //---Options
    allow_caching:options.allow_caching,
    base_window_title:options.base_window_title,
    tableofcontents:options.tableofcontents,  // Show Table of contents
    afterPageLoad:options.afterPageLoad,
    
    //---Markdown Parsing
    mdRender: function(text) {
        // Render Markdown
        document.getElementById(MDPage.md_element).innerHTML=cmarkdRender(text)
        
        // Get Page Title
        let title_elm = document.querySelector('#'+MDPage.md_element+' h1')
        let title_txt = MDPage.base_window_title
        if (title_elm) {
            if (title_txt != '') {title_txt += ' | '}
            title_txt+=title_elm.innerText
            if (document.getElementById('page_title')) {
                document.getElementById('page_title').innerText=title_elm.innerText
            }
            title_elm.style.display='none'
        }
        document.title = title_txt
        
        // Load Table of Contents
        if (MDPage.tableofcontents) {
            MDPage.loadTOC()
        }
        MDPage.toggleTOC(MDPage.tableofcontents)
    },
    
    loadTOC: function() {
        if (document.getElementById('page_toc')) {
            document.getElementById('page_toc').innerHTML = ''
            let h_elms = document.getElementById(MDPage.md_element).querySelectorAll('h2,h3,h4,h5')
            for (let elm of h_elms) {
                let telm = document.createElement('a')
                telm.innerText = elm.innerText
                // telm.className = 'toc-item'
                telm.style = 'margin-left:'+16*parseInt(elm.tagName.slice(1)-1)+'px'
                telm.onclick=function(){
                    elm.scrollIntoView()
                }
                document.getElementById('page_toc').appendChild(telm)
            }
        }
    },

    toggleTOC: function(show) {
        MDPage.tableofcontents=show
        if (document.getElementById('page_toc') && document.getElementById('left_container')) {
            if (!show) {
                document.getElementById('page_toc').style.display='none'
                document.getElementById('left_container').style['min-width']='0'
                MDPage.loadTOC()
            } else {
                document.getElementById('page_toc').style.display='block'
                document.getElementById('left_container').style['min-width']='220px'
            }
        }
    },

    //---
    //---Other Rendering
    textRender:function(text) {
        document.getElementById(MDPage.md_element).innerText=text
        document.getElementById(MDPage.md_element).style['white-space']='pre'
        
        // Page Title
        let page = MDPage.getQueryArg('page')
        
        let title_txt = MDPage.base_window_title
        if (page) {
            page = page.split('/').slice(-1)[0]
            if (title_txt != '') {title_txt += ' | '}
            title_txt+=page
            if (document.getElementById('page_title')) {
                document.getElementById('page_title').innerText=page
            }
        }
        document.title = title_txt
    },
    
    replaceAll: function(string, search_val, replace_val) {
        return string.split(search_val).join(replace_val)
    },
    
    //---
    //---Loading
    loadUrl: async function(url) {
        let ext = url.split('.').slice(-1)[0]
        // Add time to url if not caching
        if (!MDPage.allow_caching) {
            if (url.indexOf('?') > -1) {url += '&'
            } else {url += '?'}
            url+='v='+(new Date()).getTime()
        }
        let resp = await fetch(url)
        let txt = await resp.text()
        document.getElementsByTagName('base')[0].setAttribute("href", url);
        console.log(ext)
        if (ext in MDPage.renderMap) {
            MDPage.renderMap[ext](txt)
        }
        // MDPage.render(txt)
        MDPage.afterPageLoad()
    },
    
    getQueryArg: function(arg) {
        let query_args = window.location.search.replace('?','').split('&')
        for (let qarg of query_args) {
            if (qarg.startsWith(arg+'=')) {
                return qarg.split('=')[1]
            }
        }
    },
    
    load: function() {
        //---f:
        window.addEventListener('load', function () {
            let edit = 0
            // let url
            // // Check if Page Specified and load
            // let query_args = window.location.search.replace('?','').split('&')
            // for (let qarg of query_args) {
            //     if (qarg.startsWith('page')) {
            //         url = qarg.split('=')[1]
            //     }
            // }
            let url = MDPage.getQueryArg('page')
            
            // Default to index
            if (url === undefined) {url = 'index.md'}
            
            // Check Link Navigation
            document.onclick = function(event) {
                let elm = event.target
                if (elm.tagName == 'A') {
                    let href = elm.href
                    let ext = href.split('/').slice(-1)[0].split('.').slice(-1)[0].split('?')[0].split('#')[0]
                    if (ext in MDPage.renderMap) {
                    // if (href.endsWith('.md')) {
                        // if (href.startsWith(document.location.origin)) {
                        //     // href = href.slice(document.location.origin.length)
                        //     href = href.slice((document.location.origin+document.location.pathname).length)
                        // }
                        href = elm.getAttribute('href')
                        if (event.ctrlKey) {
                            window.open(document.location.pathname+'?page='+href)
                        } else {
                            document.location.href=document.location.pathname+'?page='+href
                        }
                        return false
                    }
                }
            }
            
            // Load the page
            if (url) {
                MDPage.loadUrl(url)
            }
            
        })
    },

}

// Map rendering functions to extension
//---o:
MDPage.renderMap = {
    'md':MDPage.mdRender,
    'LICENSE':MDPage.textRender,
    'txt':MDPage.textRender,
}

return MDPage
}
